package mk.ukim.finki.wp.nns.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.Hibernate;

import java.util.List;
import java.util.Objects;
import mk.ukim.finki.wp.nns.model.enumerations.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Professor
{
    @Id
    @Column(name = "id")
    private String id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "email", nullable = false, unique = true)
    private String email;

    @Enumerated(EnumType.STRING)
    private ProfessorTitle title;

    private Short orderingRank;

    @ManyToOne
    private Room office;

    @OneToMany(mappedBy = "mentionedProfessor", cascade = CascadeType.ALL)
    @JsonManagedReference
    private List<Topic> topics;

    @OneToMany(mappedBy = "requestedByProfessor", cascade = CascadeType.ALL)
    @JsonManagedReference
    private List<Topic> requestedTopics;

    @OneToMany(mappedBy = "professor", cascade = CascadeType.ALL)
    @JsonManagedReference
    private List<CommissionMember> commission;

    @OneToMany(mappedBy = "professor", cascade = CascadeType.ALL)
    @JsonManagedReference
    private List<Comment> commentsOnTopic;

    public Professor(String name, ProfessorTitle title)
    {
        this.name = name;
        this.title = title;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Professor professor = (Professor) o;
        return getId() != null && Objects.equals(getId(), professor.getId());
    }

    @Override
    public int hashCode()
    {
        return getClass().hashCode();
    }
}
