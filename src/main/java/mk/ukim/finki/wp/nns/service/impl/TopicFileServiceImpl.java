package mk.ukim.finki.wp.nns.service.impl;


import mk.ukim.finki.wp.nns.model.TopicFile;
import mk.ukim.finki.wp.nns.repository.TopicFileRepository;
import mk.ukim.finki.wp.nns.service.TopicFileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class TopicFileServiceImpl implements TopicFileService {

    @Autowired
    private TopicFileRepository topicFileRepository;

    @Override
    public Optional<TopicFile> findById(Long id) {
        return topicFileRepository.findById(id);
    }
}
