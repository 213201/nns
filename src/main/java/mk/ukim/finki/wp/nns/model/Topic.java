package mk.ukim.finki.wp.nns.model;

import com.fasterxml.jackson.annotation.*;
import jakarta.annotation.Nullable;
import jakarta.persistence.*;
import lombok.*;
import mk.ukim.finki.wp.nns.model.enumerations.*;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Topic
{
    @Id
    @GeneratedValue
    private Long id;

    private String serialNumber;

    @Enumerated(EnumType.STRING)
    private TopicCategory categoryName;

    @Enumerated(EnumType.STRING)
    private TopicStatus topicStatus = TopicStatus.SCHEDULED;

    private String subCategoryName;

    @Column(length = 6000)
    private String description;

    private Boolean isAccepted = Boolean.TRUE;

    private Integer acceptNumber;
    private Integer againstNumber;
    private Integer sustainedNumber;

    private String discussion = "";
    private String fileURL;

    @OneToMany(mappedBy = "topic", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    private List<Comment> comments;

    @ManyToOne
    @JsonBackReference
    private Professor requestedByProfessor;

    @ManyToOne
    @JsonBackReference
    private TeachingAndScientificMeeting meeting;

    @ManyToOne
    @JsonBackReference
    private Student mentionedStudent;

    @ManyToOne
    @JsonBackReference
    private Professor mentionedProfessor;
    private String mentionedProfessorDisplayName;

    @ManyToMany
    @ToString.Exclude
    @JsonBackReference
    private List<CommissionMember> commissionMembers;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    private TopicFile file;

    @Nullable
    private Boolean isVotable = true;


    public Topic(String serialNumber, TopicCategory categoryName,
                 String subCategoryName,
                 TeachingAndScientificMeeting meeting, Student mentionedStudent,
                 Professor mentionedProfessor, List<CommissionMember> commissionMembers,
                 TopicFile file, String fileURL, TopicStatus status, Professor requestedBy, Boolean isVotable)
    {
        this.topicStatus = status;
        this.serialNumber = serialNumber;
        this.categoryName = categoryName;
        this.subCategoryName = subCategoryName;
        this.comments = new ArrayList<>();

        switch (categoryName){
            case STAFF:
                this.description = "staff description";
                break;
            case PHD:
                this.description = "third degree of studies";
                break;
            case MASTER:
                this.description = "second degree of studies";
                break;
            case TEACHING:
                this.description = "teaching description";
                break;
            case FINANCE:
                this.description = "finance description";
                break;
            case OTHER:
                this.description = "other description";
                break;
        }

        this.meeting = meeting;
        this.requestedByProfessor = requestedBy;
        this.mentionedStudent = mentionedStudent;
        this.mentionedProfessor = mentionedProfessor;
        this.commissionMembers = commissionMembers;
        this.file = file;
        this.fileURL = fileURL;
        if(mentionedProfessor != null)
            this.mentionedProfessorDisplayName = mentionedProfessor.getTitle().typeName() + " " + mentionedProfessor.getName();
        this.isVotable = isVotable;
    }

}
