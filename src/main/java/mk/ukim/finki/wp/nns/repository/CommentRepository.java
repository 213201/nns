package mk.ukim.finki.wp.nns.repository;

import mk.ukim.finki.wp.nns.model.Comment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CommentRepository extends JpaRepository<Comment, Long>
{

}
