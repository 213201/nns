package mk.ukim.finki.wp.nns.repository;

import mk.ukim.finki.wp.nns.model.Professor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ProfessorRepository extends JpaSpecificationRepository<Professor, String>
{
    @Query("SELECT p FROM Professor p ORDER BY p.name ASC")
    List<Professor> findAllSortedDesc();

    Page<Professor> findAllByOrderByNameAsc(Pageable Pageable);
}

