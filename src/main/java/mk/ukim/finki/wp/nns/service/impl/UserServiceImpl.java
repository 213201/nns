package mk.ukim.finki.wp.nns.service.impl;

import mk.ukim.finki.wp.nns.model.User;
import mk.ukim.finki.wp.nns.repository.UserRepository;
import mk.ukim.finki.wp.nns.service.UserService;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService
{
    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository)
    {
        this.userRepository = userRepository;
    }

    @Override
    public User findById(String id)
    {
        return this.userRepository.findById(id).orElseThrow();
    }
}
