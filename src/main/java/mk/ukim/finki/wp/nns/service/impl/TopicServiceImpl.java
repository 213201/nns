package mk.ukim.finki.wp.nns.service.impl;

import mk.ukim.finki.wp.nns.model.*;
import mk.ukim.finki.wp.nns.model.enumerations.CommissionMemberType;
import mk.ukim.finki.wp.nns.model.enumerations.TopicCategory;
import mk.ukim.finki.wp.nns.model.enumerations.TopicStatus;
import mk.ukim.finki.wp.nns.repository.*;
import mk.ukim.finki.wp.nns.service.MeetingService;
import mk.ukim.finki.wp.nns.service.TopicService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class TopicServiceImpl implements TopicService
{
    private final TopicRepository topicRepository;
    private final MeetingService meetingService;
    private final StudentRepository studentRepository;
    private final ProfessorRepository professorRepository;
    private final CommissionMemberRepository commissionMemberRepository;
    private final TopicFileRepository topicFileRepository;
    private final MeetingRepository meetingRepository;

    public TopicServiceImpl(TopicRepository topicRepository, MeetingService meetingService, StudentRepository studentRepository, ProfessorRepository professorRepository, CommissionMemberRepository commissionMemberRepository, TopicFileRepository topicFileRepository, MeetingRepository meetingRepository)
    {
        this.topicRepository = topicRepository;
        this.meetingService = meetingService;
        this.studentRepository = studentRepository;
        this.professorRepository = professorRepository;
        this.commissionMemberRepository = commissionMemberRepository;
        this.topicFileRepository = topicFileRepository;
        this.meetingRepository = meetingRepository;
    }

    @Override
    public List<Topic> findAll()
    {
        return topicRepository.findAll();
    }

    @Override
    public Topic findById(Long id)
    {
        return topicRepository.findById(id).orElseThrow();
    }
    public Long findPreviousId(Long id)
    {
        Topic originalTopic = topicRepository.findById(id).orElseThrow();

        String meetingId = originalTopic.getMeeting().getMeetingNumberEncoded();

        TeachingAndScientificMeeting meeting = meetingRepository.findById(meetingId).orElseThrow();

        List<Topic> topics = meeting.getTopicsList().stream()
                .filter(t -> t.getTopicStatus() != TopicStatus.REQUESTED)
                .sorted(Comparator.comparing(Topic::getSerialNumber))
                .toList();

        int originalTopicIndex = topics.indexOf(originalTopic);

        if(originalTopicIndex != -1 && originalTopicIndex != 0)
        {
            int previousTopicIndex = originalTopicIndex - 1;

            Topic previousTopic = topics.get(previousTopicIndex);

            return previousTopic.getId();
        }

        return null;
    }
    public Long findFollowingId(Long id)
    {
        Topic originalTopic = topicRepository.findById(id).orElseThrow();

        String meetingId = originalTopic.getMeeting().getMeetingNumberEncoded();

        TeachingAndScientificMeeting meeting = meetingRepository.findById(meetingId).orElseThrow();

        List<Topic> topics = meeting.getTopicsList().stream()
                .filter(t -> t.getTopicStatus() != TopicStatus.REQUESTED)
                .sorted(Comparator.comparing(Topic::getSerialNumber))
                .toList();

        int originalTopicIndex = topics.indexOf(originalTopic);

        if(originalTopicIndex != -1 && originalTopicIndex < (topics.size() - 1))
        {
            int followingTopicIndex = originalTopicIndex + 1;

            Topic followingTopic = topics.get(followingTopicIndex);

            return followingTopic.getId();
        }

        return null;
    }

    @Override
    public Topic updateFromPresentingMode(Long id,
                                          Integer acceptNumber,
                                          Integer againstNumber,
                                          Integer sustainedNumber,
                                          String discussion) {
        Topic topic = topicRepository.findById(id).orElseThrow();

        if(topic.getIsVotable()){
            if(topic.getTopicStatus() != (TopicStatus.FINISHED) && topic.getTopicStatus() != (TopicStatus.CANCELED)){
                Integer eligibleMembers = topic.getMeeting().getEligibleMembers();

                if(acceptNumber == 0){
                    eligibleMembers = topic.getMeeting().getEligibleMembers();
                    acceptNumber =  eligibleMembers - againstNumber - sustainedNumber;
                }

                topic.setAcceptNumber(acceptNumber);
                topic.setAgainstNumber(againstNumber);
                topic.setSustainedNumber(sustainedNumber);
                Double acceptPercentage = Double.valueOf(acceptNumber) / Double.valueOf(eligibleMembers);
                if(acceptPercentage >= 0.5)
                    topic.setTopicStatus(TopicStatus.FINISHED);

                else
                    topic.setTopicStatus(TopicStatus.CANCELED);
            }
        }

        else{
            topic.setTopicStatus(TopicStatus.FINISHED);
        }

//        if(topic.getTopicStatus() != TopicStatus.FINISHED && topic.getTopicStatus() != TopicStatus.CANCELED) {
        topic.setDiscussion(discussion);
//        }

        return topicRepository.save(topic);
    }

    @Transactional
    @Override
    public Topic create(TopicCategory categoryName, String subCategoryName,
                        String serialNumber, String nnsMeetingId,
                        String index, String professorId, String mentorId,
                        String commissionPresidentId, List<String> commissionMembersIds,byte[] fileContent,String fileName, String fileURL, Boolean isVotable)
    {
        TeachingAndScientificMeeting meeting = meetingService.findById(nnsMeetingId);
        Student student = studentRepository.findById(index).orElse(null);
        Professor professor = professorRepository.findById(professorId).orElse(null);
        Professor commissionPresidentProfessor = professorRepository.findById(commissionPresidentId).orElse(null);
        Professor mentorProfessorObject = (student != null) ? professorRepository.findById(mentorId).orElse(null) : null;

        List<CommissionMember> commissionMembers = new ArrayList<>();

        if(commissionMembersIds != null && !commissionMembersIds.isEmpty())
        {
            commissionMembers.addAll(new ArrayList<>(commissionMembersIds.stream().map(id ->
            {
                Professor commissiomMemberProfessor = professorRepository.findById(id).orElseThrow();
                CommissionMember member = commissionMemberRepository.findCommissionMemberByProfessorAndType(commissiomMemberProfessor, CommissionMemberType.MEMBER).orElse(null);

                if (member == null)
                {
                    member = new CommissionMember(commissiomMemberProfessor, CommissionMemberType.MEMBER);
                    commissionMemberRepository.save(member);
                }

                return member;
            }).toList()));

        }

        if(mentorProfessorObject != null)
        {
            CommissionMember mentorObj = commissionMemberRepository.findCommissionMemberByProfessorAndType(mentorProfessorObject, CommissionMemberType.MENTOR).orElse(null);
            if (mentorObj == null)
            {
                mentorObj = new CommissionMember(mentorProfessorObject, CommissionMemberType.MENTOR);
                commissionMemberRepository.save(mentorObj);
            }
            commissionMembers.add(mentorObj);
        }

        CommissionMember commissionPresident = commissionMemberRepository.findCommissionMemberByProfessorAndType(commissionPresidentProfessor, CommissionMemberType.PRESIDENT).orElse(null);
        if (commissionPresident == null && commissionPresidentProfessor != null)
        {
            commissionPresident = new CommissionMember(commissionPresidentProfessor, CommissionMemberType.PRESIDENT);
            commissionMemberRepository.save(commissionPresident);

            commissionMembers.add(commissionPresident);
        }
        else if(commissionPresident != null)
            commissionMembers.add(commissionPresident);

        Topic topic = new Topic(serialNumber,categoryName,subCategoryName,meeting,student,professor,commissionMembers,
                null, fileURL, TopicStatus.SCHEDULED, null, isVotable);
        if(!fileName.isEmpty())
        {
            TopicFile file = new TopicFile(fileName,fileContent);
            topicFileRepository.save(file);
            topic = new Topic(serialNumber,categoryName,subCategoryName,meeting,student,professor,commissionMembers,
                    file, fileURL, TopicStatus.SCHEDULED, null, isVotable);
        }


        return topicRepository.save(topic);
    }

    @Transactional
    @Override
    public Topic request(TopicCategory categoryName, String subCategoryName,
                        String serialNumber, String nnsMeetingId,
                        String index, String professorId, String mentorId,
                        String commissionPresidentId, List<String> commissionMembersIds,
                        byte[] fileContent,String fileName, String fileURL, String requestedById, Boolean isVotable)
    {
        TeachingAndScientificMeeting meeting = meetingService.findById(nnsMeetingId);
        Student student = studentRepository.findById(index).orElse(null);
        Professor professor = professorRepository.findById(professorId).orElse(null);
        Professor commissionPresidentProfessor = professorRepository.findById(commissionPresidentId).orElse(null);
        Professor mentorProfessorObject = (student != null) ? professorRepository.findById(mentorId).orElse(null) : null;
        Professor requestedBy = professorRepository.findById(requestedById).orElse(null);

        List<CommissionMember> commissionMembers = new ArrayList<>();

        if(commissionMembersIds != null && !commissionMembersIds.isEmpty())
        {
            commissionMembers.addAll(new ArrayList<>(commissionMembersIds.stream().map(id ->
            {
                Professor commissiomMemberProfessor = professorRepository.findById(id).orElseThrow();
                CommissionMember member = commissionMemberRepository.findCommissionMemberByProfessorAndType(commissiomMemberProfessor, CommissionMemberType.MEMBER).orElse(null);

                if (member == null)
                {
                    member = new CommissionMember(commissiomMemberProfessor, CommissionMemberType.MEMBER);
                    commissionMemberRepository.save(member);
                }

                return member;
            }).toList()));

        }

        if(mentorProfessorObject != null)
        {
            CommissionMember mentorObj = commissionMemberRepository.findCommissionMemberByProfessorAndType(mentorProfessorObject, CommissionMemberType.MENTOR).orElse(null);
            if (mentorObj == null)
            {
                mentorObj = new CommissionMember(mentorProfessorObject, CommissionMemberType.MENTOR);
                commissionMemberRepository.save(mentorObj);
            }
            commissionMembers.add(mentorObj);
        }

        CommissionMember commissionPresident = commissionMemberRepository.findCommissionMemberByProfessorAndType(commissionPresidentProfessor, CommissionMemberType.PRESIDENT).orElse(null);
        if (commissionPresident == null && commissionPresidentProfessor != null)
        {
            commissionPresident = new CommissionMember(commissionPresidentProfessor, CommissionMemberType.PRESIDENT);
            commissionMemberRepository.save(commissionPresident);

            commissionMembers.add(commissionPresident);
        }
        else if(commissionPresident != null)
            commissionMembers.add(commissionPresident);

        Topic topic = new Topic(serialNumber,categoryName,subCategoryName,meeting,student,professor,commissionMembers,
                null, fileURL, TopicStatus.REQUESTED, requestedBy, isVotable);
        if(!fileName.isEmpty())
        {
            TopicFile file = new TopicFile(fileName,fileContent);
            topicFileRepository.save(file);
            topic = new Topic(serialNumber,categoryName,subCategoryName,meeting,student,professor,commissionMembers,
                    file, fileURL, TopicStatus.REQUESTED, requestedBy, isVotable);
        }


        return topicRepository.save(topic);
    }

    @Override
    public Topic update(Long id, TopicCategory categoryName, TopicStatus topicStatus, String subCategoryName,
                        Integer acceptNumber,Integer againstNumber, Integer sustainedNumber,
                        String description, String serialNumber, Boolean isAccepted,
                        String discussion, String nnsMeetingId, String index,
                        String professorId, String mentorId, String commissionPresidentId, List<String> commissionMembersIds,byte[] fileContent,String fileName, String fileURL, Boolean isVotable) {

        Topic topic = topicRepository.findById(id).orElseThrow();
        TeachingAndScientificMeeting meeting = meetingService.findById(nnsMeetingId);
        Optional<Student> student = studentRepository.findById(index);
        Optional<Professor> professor = professorRepository.findById(professorId);
        Professor mentorProfessorObject = (student.isPresent()) ? professorRepository.findById(mentorId).orElse(null) : null;

        if(isAccepted == null)
            isAccepted = false;

        Professor commissionPresidentProf = professorRepository.findById(commissionPresidentId).orElse(null);

        List<CommissionMember> members = new ArrayList<>();

        CommissionMember commissionPresident = commissionMemberRepository.findCommissionMemberByProfessorAndType(commissionPresidentProf, CommissionMemberType.PRESIDENT).orElse(null);
        if (commissionPresident == null && commissionPresidentProf != null)
        {
            commissionPresident = new CommissionMember(commissionPresidentProf, CommissionMemberType.PRESIDENT);
            commissionMemberRepository.save(commissionPresident);

            members.add(commissionPresident);
        }
        else if(commissionPresident != null)
            members.add(commissionPresident);

        if(commissionMembersIds != null && !commissionMembersIds.isEmpty())
        {
            members.addAll(new ArrayList<>(commissionMembersIds.stream().map(memberId ->
            {
                Professor commissionMemberProfessor = professorRepository.findById(memberId).orElseThrow();
                CommissionMember member = commissionMemberRepository.findCommissionMemberByProfessorAndType(commissionMemberProfessor, CommissionMemberType.MEMBER).orElse(null);

                if (member == null)
                {
                    member = new CommissionMember(commissionMemberProfessor, CommissionMemberType.MEMBER);
                    commissionMemberRepository.save(member);
                }

                return member;
            }).toList()));
        }

        if(mentorProfessorObject != null)
        {
            CommissionMember mentorObj = commissionMemberRepository.findCommissionMemberByProfessorAndType(mentorProfessorObject, CommissionMemberType.MENTOR).orElse(null);
            if (mentorObj == null)
            {
                mentorObj = new CommissionMember(mentorProfessorObject, CommissionMemberType.MENTOR);
                commissionMemberRepository.save(mentorObj);
            }
            members.add(mentorObj);
        }

        topic.setCategoryName(categoryName);
        topic.setTopicStatus(topicStatus);

        if(topic.getTopicStatus().equals(TopicStatus.SCHEDULED) || topic.getTopicStatus().equals(TopicStatus.FINISHED)){
            if((acceptNumber == null || acceptNumber == 0) && (againstNumber == null || againstNumber == 0) && (sustainedNumber == null || sustainedNumber == 0)) {
                topic.setTopicStatus(TopicStatus.SCHEDULED);
            } else {
                topic.setTopicStatus(TopicStatus.FINISHED);
            }
        }
        topic.setSubCategoryName(subCategoryName);
        topic.setAcceptNumber(acceptNumber);
        topic.setAgainstNumber(againstNumber);
        topic.setSustainedNumber(sustainedNumber);
        topic.setDescription(description);
        topic.setSerialNumber(serialNumber);
        topic.setIsAccepted(isAccepted);
        topic.setDiscussion(discussion);
        topic.setMeeting(meeting);
        topic.setCommissionMembers(members);
        topic.setIsVotable(isVotable);

        if (fileContent != null && fileName != null) {
            TopicFile topicFile = topic.getFile();
            if (topicFile == null) {
                topicFile = new TopicFile();
            }
            topicFile.setContent(fileContent);
            topicFile.setFileName(fileName);
            topic.setFile(topicFile);
        }

        if (fileURL != null) {
            topic.setFileURL(fileURL);
        }

        if(student.isPresent())
            topic.setMentionedStudent(student.get());
        else
            topic.setMentionedStudent(null);

        if(professor.isPresent())
            topic.setMentionedProfessor(professor.get());
        else
            topic.setMentionedProfessor(null);

        return topicRepository.save(topic);
    }

    @Override
    public Set<String> getAllSubCategories()
    {
        return findAll().stream().map(Topic::getSubCategoryName).filter(subCategory -> !subCategory.isEmpty()).collect(Collectors.toSet());
    }


    @Override
    public Topic remove(Long id)
    {
        Topic topic = topicRepository.findById(id).orElseThrow();
        topicRepository.delete(topic);
        return topic;
    }
    @Override
    public Topic removeFile(Long id)
    {
        Topic topic = topicRepository.findById(id).orElseThrow();
        if(topic.getFile() != null)
        {
            topicFileRepository.delete(topic.getFile());
            topic.setFile(null);
        }

        topicRepository.save(topic);

        return topic;
    }

    @Override
    public Topic approve(Long id)
    {
        Topic topic = topicRepository.findById(id).orElseThrow();

        topic.setTopicStatus(TopicStatus.SCHEDULED);

        return topicRepository.save(topic);
    }

    @Override
    public Long countRequests(String meetingId){
        Long count = topicRepository.findAll().stream()
                .filter(s -> s.getMeeting().getMeetingNumber().equals(meetingId) && s.getTopicStatus() == TopicStatus.REQUESTED)
                .count();

        return count;
    }




}
