package mk.ukim.finki.wp.nns.service.impl;

import mk.ukim.finki.wp.nns.model.Room;
import mk.ukim.finki.wp.nns.repository.RoomRepository;
import mk.ukim.finki.wp.nns.service.RoomService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoomServiceImpl implements RoomService
{

    private final RoomRepository roomRepository;

    public RoomServiceImpl(RoomRepository _roomRepository)
    {
        this.roomRepository = _roomRepository;
    }

    @Override
    public List<Room> findAll()
    {
        return roomRepository.findAll();
    }
}
