package mk.ukim.finki.wp.nns.model;


import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.Type;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class TopicFile {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String fileName;

    @Lob
    private byte[] content;

    public TopicFile(String fileName, byte[] content) {
        this.fileName = fileName;
        this.content = content;
    }
}
