package mk.ukim.finki.wp.nns.service.impl;

import mk.ukim.finki.wp.nns.model.Comment;
import mk.ukim.finki.wp.nns.model.CommissionMember;
import mk.ukim.finki.wp.nns.model.Professor;
import mk.ukim.finki.wp.nns.model.Topic;
import mk.ukim.finki.wp.nns.repository.CommentRepository;
import mk.ukim.finki.wp.nns.repository.CommissionMemberRepository;
import mk.ukim.finki.wp.nns.repository.ProfessorRepository;
import mk.ukim.finki.wp.nns.repository.TopicRepository;
import mk.ukim.finki.wp.nns.service.CommentSerivce;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class CommentServiceImpl implements CommentSerivce
{

    private final CommentRepository commentRepository;
    private final TopicRepository topicRepository;
    private final ProfessorRepository professorRepository;

    public CommentServiceImpl(CommentRepository commentRepository, TopicRepository topicRepository, ProfessorRepository professorRepository)
    {
        this.commentRepository = commentRepository;
        this.topicRepository = topicRepository;
        this.professorRepository = professorRepository;
    }


    @Override
    public List<Comment> findAll()
    {
        return commentRepository.findAll();
    }

    @Override
    public Optional<Comment> findById(Long id)
    {
        return commentRepository.findById(id);
    }

    @Override
    public Comment create(String commentText, Long topicId, String memberId)
    {
        Professor professor = professorRepository.findById(memberId).orElseThrow();
        Topic topic = topicRepository.getById(topicId);

        Comment comment = new Comment(commentText, topic, LocalDateTime.now(), professor);

        return commentRepository.save(comment);
    }

    @Override
    public Comment update(Long id, String commentText)
    {
        Comment comment = this.findById(id).orElseThrow();

        comment.setCommentText(commentText);
        comment.setDatePosted(LocalDateTime.now());

        return commentRepository.save(comment);
    }

    @Override
    public Comment delete(Long id) {

        Comment comment = this.findById(id).orElseThrow();

        commentRepository.delete(comment);

        return comment;
    }

}
