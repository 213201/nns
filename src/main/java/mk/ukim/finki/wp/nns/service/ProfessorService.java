package mk.ukim.finki.wp.nns.service;

import mk.ukim.finki.wp.nns.model.Professor;
import mk.ukim.finki.wp.nns.model.Topic;
import mk.ukim.finki.wp.nns.model.enumerations.CommissionMemberType;
import mk.ukim.finki.wp.nns.model.enumerations.ProfessorTitle;
import mk.ukim.finki.wp.nns.model.enumerations.TopicCategory;
import mk.ukim.finki.wp.nns.model.enumerations.TopicStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface ProfessorService
{
    Professor findById(String id);

    List<Professor> findAll();

    Professor create(String name, ProfessorTitle professorTitle);

    Professor update(String id,String name, String email, ProfessorTitle title, String roomName);

    Page<Professor> findAllWithPagination(Pageable pageable);

    List<Topic> findAllTopicsWhereProfessorIsMentionedSortedByMeetingDate(Professor professorOfInterest);

    List<Topic> findAllTopicsWhereProfessorIsCommissionSortedByMeetingDate(Professor professorOfInterest, CommissionMemberType type);

    List<Topic> findAllTopicsWhereProfessorIsMentionedFilteredByCategoryNameAndSubCategoryName(Professor professorOfInterest, String categoryName, String subCategoryName);

    List<Topic> findAllTopicsWhereProfessorIsCommissionFilteredByCategoryNameAndSubCategoryName(Professor professorOfInterest, CommissionMemberType type, String categoryName, String subCategoryName);
}
