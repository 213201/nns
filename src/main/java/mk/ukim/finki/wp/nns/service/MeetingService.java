package mk.ukim.finki.wp.nns.service;

import mk.ukim.finki.wp.nns.model.Room;
import mk.ukim.finki.wp.nns.model.TeachingAndScientificMeeting;

import mk.ukim.finki.wp.nns.model.Topic;
import org.jodconverter.core.office.OfficeException;
import org.json.JSONException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;

public interface MeetingService
{
    List<TeachingAndScientificMeeting> findAllSortedByDateDesc();

    Page<TeachingAndScientificMeeting> findAllWithPagination(Pageable pageable);

    List<TeachingAndScientificMeeting> findAllHeldBeforeSelectedDateDesc(LocalDateTime date);

    TeachingAndScientificMeeting findById(String id);

    TeachingAndScientificMeeting create(String meetingNumber, LocalDateTime date, String roomId);

    TeachingAndScientificMeeting update(String id, LocalDateTime date, String room, Integer presentMembers, Integer eligibleMembers);

    TeachingAndScientificMeeting remove(String id);

    List<Topic> sortAcceptedTopicsBySerialNumberForMeeting(String id);

    List<Topic> sortRequestedTopicsBySerialNumberForMeeting(String id);

    void processUploadedFile(MultipartFile meetingFile) throws JSONException, IOException, OfficeException;
}
