package mk.ukim.finki.wp.nns.repository;

import mk.ukim.finki.wp.nns.model.CommissionMember;
import mk.ukim.finki.wp.nns.model.Professor;
import mk.ukim.finki.wp.nns.model.Student;
import mk.ukim.finki.wp.nns.model.Topic;
import mk.ukim.finki.wp.nns.model.enumerations.TopicCategory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TopicRepository extends JpaRepository<Topic, Long>
{
    List<Topic> findAllByMentionedProfessor(Professor professor);

    List<Topic> findAllByMentionedProfessorAndCategoryName(Professor professor, TopicCategory topicCategory);

    List<Topic> findAllByMentionedProfessorAndSubCategoryNameIgnoreCase(Professor professor, String subCategoryName);

    List<Topic> findAllByMentionedProfessorAndCategoryNameAndSubCategoryNameIgnoreCase(Professor professor, TopicCategory topicCategory, String subCategoryName);

    List<Topic> findAllByMentionedStudent(Student student);

    List<Topic> findAllByMentionedStudentAndCategoryName(Student student, TopicCategory topicCategory);

    List<Topic> findAllByMentionedStudentAndSubCategoryNameIgnoreCase(Student student, String subCategoryName);

    List<Topic> findAllByMentionedStudentAndCategoryNameAndSubCategoryNameIgnoreCase(Student student, TopicCategory topicCategory, String subCategoryName);

    List<Topic> findAllByCommissionMembersContaining(CommissionMember commissionMember);

    List<Topic> findAllByCommissionMembersContainingAndCategoryName(CommissionMember commissionMember, TopicCategory topicCategory);

    List<Topic> findAllByCommissionMembersContainingAndSubCategoryNameIgnoreCase(CommissionMember commissionMember, String subCategoryName);

    List<Topic> findAllByCommissionMembersContainingAndCategoryNameAndSubCategoryNameIgnoreCase(CommissionMember commissionMember, TopicCategory topicCategory, String subCategoryName);
}
