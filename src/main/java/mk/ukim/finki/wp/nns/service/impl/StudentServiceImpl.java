package mk.ukim.finki.wp.nns.service.impl;

import lombok.AllArgsConstructor;
import mk.ukim.finki.wp.nns.model.Student;
import mk.ukim.finki.wp.nns.model.Topic;
import mk.ukim.finki.wp.nns.model.enumerations.TopicCategory;
import mk.ukim.finki.wp.nns.repository.StudentRepository;
import mk.ukim.finki.wp.nns.repository.TopicRepository;
import mk.ukim.finki.wp.nns.service.StudentService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;

@Service
@AllArgsConstructor
public class StudentServiceImpl implements StudentService
{
    private final StudentRepository studentRepository;
    private final TopicRepository topicRepository;

    @Override
    public Student findById(String id) {
        return studentRepository.findById(id).orElseThrow();
    }

    @Override
    public List<Student> findAll() {
        return studentRepository.findAll();
    }

    @Override
    public Page<Student> findAllWithPagination(Pageable pageable) {
        return studentRepository.findAllByOrderByNameAsc(pageable);
    }

    @Override
    public List<Topic> findAllTopicsWhereStudentIsMentionedSortedByMeetingDate(Student student) {
        return student
                .getTopics()
                .stream()
                .sorted(Comparator.<Topic, LocalDateTime>comparing(topic -> topic.getMeeting().getDate()).reversed())
                .toList();
    }

    @Override
    public List<Topic> findAllTopicsWhereStudentIsMentionedFilteredByCategoryNameAndSubCategoryName(Student student, String categoryName, String subCategoryName) {
        if(categoryName.equalsIgnoreCase("Сите") && subCategoryName.equals("")){
            return topicRepository.findAllByMentionedStudent(student);
        }

        if(!categoryName.equalsIgnoreCase("сите") && !subCategoryName.equals("")){
            return topicRepository.findAllByMentionedStudentAndCategoryNameAndSubCategoryNameIgnoreCase(student, TopicCategory.valueOf(categoryName), subCategoryName);
        }

        if(!categoryName.equalsIgnoreCase("сите")){
            return topicRepository.findAllByMentionedStudentAndCategoryName(student, TopicCategory.valueOf(categoryName));
        }

        else
            return topicRepository.findAllByMentionedStudentAndSubCategoryNameIgnoreCase(student, subCategoryName);

    }
}
