package mk.ukim.finki.wp.nns.model;

import com.fasterxml.jackson.annotation.*;
import jakarta.persistence.*;
import lombok.*;

import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class TeachingAndScientificMeeting
{
    @Id
    private String meetingNumber;

    private LocalDateTime date;

    private Integer presentMembers;
    private Integer eligibleMembers;

    @ManyToOne
    private Room room;

    @ManyToOne
    private User chairMan;

    @OneToMany(mappedBy = "meeting", cascade = CascadeType.ALL)
    @JsonManagedReference
    private List<Topic> topicsList;

    public TeachingAndScientificMeeting(String meetingNumber, LocalDateTime date, Room room, User chairMan)
    {
        this.meetingNumber = meetingNumber;
        this.date = date;
        this.room = room;
        this.chairMan = chairMan;
    }

    public String getMeetingNumber() {
        return URLDecoder.decode(this.meetingNumber, StandardCharsets.UTF_8);
    }
    public String getMeetingNumberEncoded() {
        return this.meetingNumber;
    }
}
