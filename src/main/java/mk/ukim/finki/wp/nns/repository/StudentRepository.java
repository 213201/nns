package mk.ukim.finki.wp.nns.repository;

import mk.ukim.finki.wp.nns.model.Professor;
import mk.ukim.finki.wp.nns.model.Student;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepository extends JpaRepository<Student,String>
{
    Page<Student> findAllByOrderByNameAsc(Pageable Pageable);
}
