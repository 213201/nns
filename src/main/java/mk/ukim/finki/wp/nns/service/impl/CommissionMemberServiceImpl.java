package mk.ukim.finki.wp.nns.service.impl;

import mk.ukim.finki.wp.nns.model.CommissionMember;
import mk.ukim.finki.wp.nns.model.Professor;
import mk.ukim.finki.wp.nns.repository.CommissionMemberRepository;
import mk.ukim.finki.wp.nns.repository.ProfessorRepository;
import mk.ukim.finki.wp.nns.service.CommissionMemberService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommissionMemberServiceImpl implements CommissionMemberService
{
    private final CommissionMemberRepository commissionMemberRepository;
    private final ProfessorRepository professorRepository;

    public CommissionMemberServiceImpl(CommissionMemberRepository commissionMemberRepository, ProfessorRepository professorRepository)
    {
        this.commissionMemberRepository = commissionMemberRepository;
        this.professorRepository = professorRepository;
    }

    @Override
    public List<CommissionMember> findAll()
    {
        return commissionMemberRepository.findAll();
    }

    @Override
    public List<CommissionMember> findAllByProfessor(String professorId)
    {
        Professor professor = professorRepository.findById(professorId).orElseThrow();
        List<CommissionMember> commission = commissionMemberRepository.findAllByProfessor(professor);

        return commission;
    }
}
