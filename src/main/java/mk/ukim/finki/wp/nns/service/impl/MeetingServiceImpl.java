package mk.ukim.finki.wp.nns.service.impl;

import mk.ukim.finki.wp.nns.model.Room;
import mk.ukim.finki.wp.nns.model.TeachingAndScientificMeeting;
import mk.ukim.finki.wp.nns.model.Topic;
import mk.ukim.finki.wp.nns.model.User;
import mk.ukim.finki.wp.nns.model.enumerations.TopicStatus;
import mk.ukim.finki.wp.nns.model.enumerations.UserRole;
import mk.ukim.finki.wp.nns.repository.MeetingRepository;
import mk.ukim.finki.wp.nns.repository.RoomRepository;
import mk.ukim.finki.wp.nns.repository.UserRepository;
import mk.ukim.finki.wp.nns.service.MeetingService;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.jodconverter.core.DocumentConverter;
import org.jodconverter.core.office.OfficeException;
import org.jodconverter.local.LocalConverter;
import org.jodconverter.local.office.LocalOfficeManager;
import org.json.JSONArray;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.io.File;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.json.JSONObject;
@Service
public class MeetingServiceImpl implements MeetingService
{

    private final MeetingRepository meetingRepository;
    private final RoomRepository roomRepository;
    private final UserRepository userRepository;

    @Value("${openAiApiKey:your-open-ai-key}")
    private String openAiApiKey;

    public MeetingServiceImpl(MeetingRepository _meetingRepository, RoomRepository _roomRepository, UserRepository _userRepository)
    {
        this.meetingRepository = _meetingRepository;
        this.roomRepository = _roomRepository;
        this.userRepository = _userRepository;
    }

    @Override
    public List<TeachingAndScientificMeeting> findAllSortedByDateDesc()
    {
        return meetingRepository.findAllByOrderByDateDesc();
    }

    @Override
    public Page<TeachingAndScientificMeeting> findAllWithPagination(Pageable pageable)
    {
        return meetingRepository.findAllByOrderByDateDesc(pageable);
    }

    @Override
    public List<TeachingAndScientificMeeting> findAllHeldBeforeSelectedDateDesc(LocalDateTime date)
    {
        List<TeachingAndScientificMeeting> filteredList = findAllSortedByDateDesc();

        filteredList = filteredList.stream()
                .filter(nnsMeeting -> nnsMeeting.getDate().isBefore(date) || nnsMeeting.getDate().isEqual(date)).toList();

        return filteredList;
    }

    @Override
    public TeachingAndScientificMeeting findById(String id)
    {
        String meetingNumberEncoded = URLEncoder.encode(id, StandardCharsets.UTF_8);

        return meetingRepository.findById(meetingNumberEncoded).orElseGet(null);
    }

    @Override
    public TeachingAndScientificMeeting create(String meetingNumber, LocalDateTime date, String roomId)
    {
        Room room = roomRepository.findById(roomId).orElseThrow();
        User chairMan = userRepository.findByRole(UserRole.ADMINISTRATION_MANAGER);

        String meetingNumberEncoded = URLEncoder.encode(meetingNumber, StandardCharsets.UTF_8);

        return meetingRepository.save(new TeachingAndScientificMeeting(meetingNumberEncoded, date, room, chairMan));
    }

    @Override
    public TeachingAndScientificMeeting update(String id, LocalDateTime date, String room, Integer presentMembers, Integer eligibleMembers)
    {
        TeachingAndScientificMeeting nnsMeeting = findById(id);
        Room roomExist = roomRepository.findById(room).orElseThrow();

        nnsMeeting.setDate(date);
        nnsMeeting.setRoom(roomExist);
        nnsMeeting.setPresentMembers(presentMembers);
        nnsMeeting.setEligibleMembers(eligibleMembers);

        return meetingRepository.save(nnsMeeting);
    }

    @Override
    public TeachingAndScientificMeeting remove(String id)
    {
        TeachingAndScientificMeeting nnsMeeting = findById(id);
        meetingRepository.delete(nnsMeeting);

        return nnsMeeting;
    }

    @Override
    public List<Topic> sortAcceptedTopicsBySerialNumberForMeeting(String id)
    {
        TeachingAndScientificMeeting nnsMeeting = findById(id);

        List<Topic> topics = nnsMeeting.getTopicsList().stream()
                .filter(t -> t.getTopicStatus() != TopicStatus.REQUESTED)
                .sorted(Comparator.comparing(Topic::getSerialNumber))
                .collect(Collectors.toList());

        return topics;
    }

    @Override
    public List<Topic> sortRequestedTopicsBySerialNumberForMeeting(String id)
    {
        TeachingAndScientificMeeting nnsMeeting = findById(id);

        List<Topic> topics = nnsMeeting.getTopicsList().stream()
                .filter(t -> t.getTopicStatus() == TopicStatus.REQUESTED)
                .sorted(Comparator.comparing(Topic::getSerialNumber))
                .collect(Collectors.toList());

        return topics;
    }


    @Override
    public void processUploadedFile(MultipartFile meetingFile) throws JSONException, IOException, OfficeException {

        if (meetingFile.getOriginalFilename() != null && meetingFile.getOriginalFilename().endsWith(".odt")) {
            meetingFile = convertOdtToPdf(meetingFile);
        }
        meetingFile = compressPdf(meetingFile);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setBearerAuth(openAiApiKey);


        Path promptFilePath = Paths.get(new ClassPathResource("prompt.txt").getURI());
        String promptMessage = Files.readString(promptFilePath);


        JSONObject payload = new JSONObject();
        payload.put("model", "gpt-3.5-turbo");

        JSONArray messagesArray = new JSONArray();
        messagesArray.put(new JSONObject().put("role", "system").put("content", promptMessage));
        messagesArray.put(new JSONObject().put("role", "user").put("content", new String(meetingFile.getBytes())));

        payload.put("messages", messagesArray);

        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<String> entity = new HttpEntity<>(payload.toString(), headers);
        ResponseEntity<String> response = restTemplate.exchange("https://api.openai.com/v1/chat/completions", HttpMethod.POST, entity, String.class);

        JSONObject responseJson = new JSONObject(response.getBody());
        String content = responseJson.getJSONArray("choices")
                .getJSONObject(0)
                .getJSONObject("message")
                .getString("content");

        String meetingNumber = extractValue(content, "meetingNumber");
        DateTimeFormatter formatter = DateTimeFormatter.ISO_LOCAL_DATE_TIME;
        LocalDateTime meetingDate = LocalDateTime.parse(Objects.requireNonNull(extractValue(content, "meetingDate")), formatter);
        String meetingLocation = "АМФ ФИНКИ";


        create(meetingNumber, meetingDate, meetingLocation);
        System.out.println("Meeting Number: " + meetingNumber);
        System.out.println("Meeting Date: " + meetingDate);
        System.out.println("Meeting Location: " + meetingLocation);
    }

    private String extractValue(String content, String key) {
        Matcher matcher = Pattern.compile("\"" + key + "\":\\s*\"([^\"]+)\"").matcher(content);
        return matcher.find() ? matcher.group(1) : null;
    }


    public MultipartFile convertOdtToPdf(MultipartFile odtFile) throws IOException, OfficeException {
        File originalOdtFile = File.createTempFile("temp", ".odt");
        odtFile.transferTo(originalOdtFile);

        File pdfFile = File.createTempFile("converted", ".pdf");

        LocalOfficeManager officeManager = LocalOfficeManager.install();
        officeManager.start();

        try {
            DocumentConverter converter = LocalConverter.make(officeManager);
            converter.convert(originalOdtFile).to(pdfFile).execute();
        } finally {
            officeManager.stop();
        }

        MultipartFile pdfMultipartFile = new MockMultipartFile(
                pdfFile.getName(),
                pdfFile.getName(),
                MediaType.APPLICATION_PDF_VALUE,
                new FileInputStream(pdfFile)
        );

        originalOdtFile.delete();
        pdfFile.delete();

        return pdfMultipartFile;
    }

    public MultipartFile compressPdf(MultipartFile pdfFile) throws IOException {
        File originalPdfFile = File.createTempFile("temp", ".pdf");
        pdfFile.transferTo(originalPdfFile);

        File compressedPdfFile = File.createTempFile("compressed", ".pdf");


        try (PDDocument document = PDDocument.load(originalPdfFile)) {
            document.setAllSecurityToBeRemoved(true);
            document.save(compressedPdfFile);
        }


        long compressedFileSize = compressedPdfFile.length();
        System.out.println("Compressed PDF file size: " + compressedFileSize + " bytes");


        MultipartFile compressedMultipartFile = new MockMultipartFile(
                compressedPdfFile.getName(),
                compressedPdfFile.getName(),
                MediaType.APPLICATION_PDF_VALUE,
                new FileInputStream(compressedPdfFile)
        );

        originalPdfFile.delete();
        compressedPdfFile.delete();

        return compressedMultipartFile;
    }


}
