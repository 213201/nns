package mk.ukim.finki.wp.nns.service;

import mk.ukim.finki.wp.nns.model.Topic;
import mk.ukim.finki.wp.nns.model.TopicFile;
import mk.ukim.finki.wp.nns.model.enumerations.TopicCategory;
import mk.ukim.finki.wp.nns.model.enumerations.TopicStatus;

import java.util.List;
import java.util.Set;

public interface TopicService
{
    List<Topic> findAll();

    Topic findById(Long id);

    Topic create(TopicCategory categoryName, String subCategoryName,
                 String serialNumber, String nnsMeetingId,
                 String index, String professorId, String mentorId, String commissionPresidentId,
                 List<String> commissionMembersIds, byte[] fileContent,String fileName, String fileURL, Boolean isVotable);

    Topic request(TopicCategory categoryName, String subCategoryName,
                 String serialNumber, String nnsMeetingId,
                 String index, String professorId, String mentorId, String commissionPresidentId,
                 List<String> commissionMembersIds, byte[] fileContent, String fileName, String fileURL,
                 String requestedById, Boolean isVotable);

    Topic update(Long id, TopicCategory categoryName, TopicStatus topicStatus, String subCategoryName,
                 Integer acceptNumber,Integer againstNumber, Integer sustainedNumber,
                 String description, String serialNumber, Boolean isAccepted,
                 String discussion, String nnsMeetingId, String index,
                 String professorId, String mentorId, String commissionPresidentId,
                 List<String> commissionMembersIds,byte[] fileContent,String fileName, String fileURL, Boolean isVotable);

    Set<String> getAllSubCategories();

    Topic remove(Long id);

    Topic removeFile(Long id);

    Topic approve(Long id);

    Long countRequests(String meetingId);

    Long findPreviousId(Long id);

    Long findFollowingId(Long id);

    Topic updateFromPresentingMode(Long id,
                                   Integer acceptNumber,
                                   Integer againstNumber,
                                   Integer sustainedNumber,
                                   String discussion);
}
