package mk.ukim.finki.wp.nns.service.impl;


import lombok.AllArgsConstructor;
import mk.ukim.finki.wp.nns.model.*;
import mk.ukim.finki.wp.nns.model.enumerations.CommissionMemberType;
import mk.ukim.finki.wp.nns.model.enumerations.ProfessorTitle;
import mk.ukim.finki.wp.nns.model.enumerations.TopicCategory;
import mk.ukim.finki.wp.nns.repository.*;
import mk.ukim.finki.wp.nns.service.ProfessorService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;

@Service
@AllArgsConstructor
public class ProfessorServiceImpl implements ProfessorService
{
    private final ProfessorRepository professorRepository;
    private final CommissionMemberRepository commissionMemberRepository;
    private final StudentRepository studentRepository;
    private final TopicRepository topicRepository;
    private final RoomRepository roomRepository;

    @Override
    public Professor findById(String id)
    {
        return professorRepository.findById(id).orElseThrow();
    }

    @Override
    public List<Professor> findAll()
    {
        return professorRepository.findAllSortedDesc();
    }

    @Override
    public Professor create(String name, ProfessorTitle professorTitle)
    {
        Professor professor = new Professor(name, professorTitle);

        return professorRepository.save(professor);
    }

    @Override
    public Professor update(String id,String name, String email, ProfessorTitle title, String roomName)
    {
        Professor professor = professorRepository.getReferenceById(id);
        Room office = roomRepository.getReferenceById(roomName);

        professor.setName(name);
        professor.setEmail(email);
        professor.setTitle(title);
        professor.setOffice(office);

        return professorRepository.save(professor);
    }

    @Override
    public Page<Professor> findAllWithPagination(Pageable pageable)
    {
        return professorRepository.findAllByOrderByNameAsc(pageable);
    }

    @Override
    public List<Topic> findAllTopicsWhereProfessorIsMentionedSortedByMeetingDate(Professor professorOfInterest)
    {
        List<Topic> allTopics = professorOfInterest.getTopics();
        List<Long> allTopicsIds = allTopics
                .stream()
                .map(Topic::getId)
                .toList();

        for(Topic topic : professorOfInterest.getTopics())
        {
            if(!allTopicsIds.contains(topic.getId()))
                allTopics.add(topic);
        }

        List<Topic> allTopicsWhereProfessorOfInterestIsMentioned = allTopics
                .stream()
                .sorted(
                        Comparator.<Topic, LocalDateTime>comparing(topic -> topic.getMeeting().getDate()).reversed()
                )
                .toList();

        return allTopicsWhereProfessorOfInterestIsMentioned;
    }

    public List<Topic> findAllTopicsWhereProfessorIsCommissionSortedByMeetingDate(Professor professorOfInterest, CommissionMemberType type)
    {
        CommissionMember member = commissionMemberRepository.findCommissionMemberByProfessorAndType(professorOfInterest, type).orElse(null);

        if(member == null)
            return null;

        List<Topic> allTopics = topicRepository.findAllByCommissionMembersContaining(member);

        List<Topic> allTopicsWhereProfessorOfInterestCommission = allTopics
                .stream()
                .sorted(
                        Comparator.<Topic, LocalDateTime>comparing(topic -> topic.getMeeting().getDate()).reversed()
                )
                .toList();

        return allTopicsWhereProfessorOfInterestCommission;
    }

    @Override
    public List<Topic> findAllTopicsWhereProfessorIsMentionedFilteredByCategoryNameAndSubCategoryName(Professor professorOfInterest, String categoryName, String subCategoryName)
    {
        if(categoryName.equalsIgnoreCase("Сите") && subCategoryName.equals(""))
            return topicRepository.findAllByMentionedProfessor(professorOfInterest)
                    .stream()
                    .sorted(
                            Comparator.<Topic, LocalDateTime>comparing(topic -> topic.getMeeting().getDate()).reversed()
                    )
                    .toList();

        if(!categoryName.equalsIgnoreCase("сите") && !subCategoryName.equals(""))
            return topicRepository.findAllByMentionedProfessorAndCategoryNameAndSubCategoryNameIgnoreCase(professorOfInterest, TopicCategory.valueOf(categoryName), subCategoryName)
                    .stream()
                    .sorted(
                            Comparator.<Topic, LocalDateTime>comparing(topic -> topic.getMeeting().getDate()).reversed()
                    )
                    .toList();

        if(!categoryName.equalsIgnoreCase("сите"))
            return topicRepository.findAllByMentionedProfessorAndCategoryName(professorOfInterest, TopicCategory.valueOf(categoryName))
                    .stream()
                    .sorted(
                            Comparator.<Topic, LocalDateTime>comparing(topic -> topic.getMeeting().getDate()).reversed()
                    )
                    .toList();

        else
            return topicRepository.findAllByMentionedProfessorAndSubCategoryNameIgnoreCase(professorOfInterest, subCategoryName)
                    .stream()
                    .sorted(
                            Comparator.<Topic, LocalDateTime>comparing(topic -> topic.getMeeting().getDate()).reversed()
                    )
                    .toList();
    }

    @Override
    public List<Topic> findAllTopicsWhereProfessorIsCommissionFilteredByCategoryNameAndSubCategoryName(Professor professorOfInterest, CommissionMemberType type, String categoryName, String subCategoryName)
    {
        CommissionMember commissionMember = commissionMemberRepository.findCommissionMemberByProfessorAndType(professorOfInterest, type).orElse(null);

        if(commissionMember == null)
            return null;

        if(categoryName.equalsIgnoreCase("сите") && subCategoryName.equals(""))
            return topicRepository.findAllByCommissionMembersContaining(commissionMember)
                    .stream()
                    .sorted(
                            Comparator.<Topic, LocalDateTime>comparing(topic -> topic.getMeeting().getDate()).reversed()
                    )
                    .toList();

        if(!categoryName.equalsIgnoreCase("сите") && !subCategoryName.equals(""))
            return topicRepository.findAllByCommissionMembersContainingAndCategoryNameAndSubCategoryNameIgnoreCase(commissionMember, TopicCategory.valueOf(categoryName), subCategoryName)
                    .stream()
                    .sorted(
                            Comparator.<Topic, LocalDateTime>comparing(topic -> topic.getMeeting().getDate()).reversed()
                    )
                    .toList();

        if(!categoryName.equalsIgnoreCase("сите"))
            return topicRepository.findAllByCommissionMembersContainingAndCategoryName(commissionMember, TopicCategory.valueOf(categoryName))
                    .stream()
                    .sorted(
                            Comparator.<Topic, LocalDateTime>comparing(topic -> topic.getMeeting().getDate()).reversed()
                    )
                    .toList();

        else
            return topicRepository.findAllByCommissionMembersContainingAndSubCategoryNameIgnoreCase(commissionMember, subCategoryName)
                    .stream()
                    .sorted(
                            Comparator.<Topic, LocalDateTime>comparing(topic -> topic.getMeeting().getDate()).reversed()
                    )
                    .toList();
    }
}
