package mk.ukim.finki.wp.nns.service;

import mk.ukim.finki.wp.nns.model.Comment;

import java.util.List;
import java.util.Optional;

public interface CommentSerivce
{
    List<Comment> findAll();

    Optional<Comment> findById(Long id);

    Comment create(String commentText, Long topicId, String memberId);

    Comment update(Long id, String commentText);

    Comment delete(Long id);
}
