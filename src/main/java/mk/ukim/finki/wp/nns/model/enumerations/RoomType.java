package mk.ukim.finki.wp.nns.model.enumerations;

public enum RoomType
{
    CLASSROOM, LAB, MEETING_ROOM, OFFICE, VIRTUAL
}
