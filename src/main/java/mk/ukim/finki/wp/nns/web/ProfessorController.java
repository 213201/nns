package mk.ukim.finki.wp.nns.web;

import lombok.AllArgsConstructor;
import mk.ukim.finki.wp.nns.model.Professor;
import mk.ukim.finki.wp.nns.model.Topic;
import mk.ukim.finki.wp.nns.model.enumerations.CommissionMemberType;
import mk.ukim.finki.wp.nns.model.enumerations.TopicCategory;
import mk.ukim.finki.wp.nns.service.ProfessorService;
import mk.ukim.finki.wp.nns.service.StudentService;
import mk.ukim.finki.wp.nns.service.TopicService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/professors")
@AllArgsConstructor
public class ProfessorController {
    private final ProfessorService professorService;
    private final TopicService topicService;
    private final StudentService studentService;

    @GetMapping
    public String getProfesorsPage(Model model,
                                   @RequestParam(required = false) Professor professorOfInterest,
                                   @RequestParam(required = false) List<Topic> topicsThatIncludeProfessorOfInterest,
                                   @RequestParam(required = false) List<Topic> topicsThatIncludeProfessorAsCommissionPresident,
                                   @RequestParam(required = false) List<Topic> topicsThatIncludeProfessorAsCommissionMember,
                                   @RequestParam(required = false) List<Topic> topicsThatIncludeProfessorAsMentor,
                                   @RequestParam(name = "page", defaultValue = "0") String pageString,
                                   @RequestParam(name = "size", defaultValue = "5") int size){
        int page;
        if(pageString.isEmpty() || pageString.equals(""))
            pageString = "0";

        try
        {
            page = Integer.parseInt(pageString);
        }
        catch (NumberFormatException e)
        {
            page = 0;
        }

        PageRequest pageable = PageRequest.of(page, size);
        Page<Professor> pagedData = professorService.findAllWithPagination(pageable);

        if(!pageString.isEmpty() && professorOfInterest == null)
            model.addAttribute("pagedData", pagedData);

        boolean showTopics = false;

        List<Professor> professorInFilter = professorService.findAll();
        List<Professor> professorsToBeShown = professorService.findAll();

        model.addAttribute("professorsInFilter", professorInFilter);

        if(topicsThatIncludeProfessorOfInterest != null && topicsThatIncludeProfessorOfInterest.isEmpty())
            topicsThatIncludeProfessorOfInterest = null;

        if(topicsThatIncludeProfessorAsCommissionPresident != null && topicsThatIncludeProfessorAsCommissionPresident.isEmpty())
            topicsThatIncludeProfessorAsCommissionPresident = null;

        if(topicsThatIncludeProfessorAsCommissionMember != null && topicsThatIncludeProfessorAsCommissionMember.isEmpty())
            topicsThatIncludeProfessorAsCommissionMember = null;

        if(topicsThatIncludeProfessorAsMentor != null && topicsThatIncludeProfessorAsMentor.isEmpty())
            topicsThatIncludeProfessorAsMentor = null;

        if((topicsThatIncludeProfessorOfInterest != null ||
                topicsThatIncludeProfessorAsCommissionMember != null ||
                topicsThatIncludeProfessorAsCommissionPresident != null ||
                topicsThatIncludeProfessorAsMentor != null))
        {
            showTopics = true;

            model.addAttribute("topics", topicsThatIncludeProfessorOfInterest);
            model.addAttribute("topicsCommissionPresident", topicsThatIncludeProfessorAsCommissionPresident);
            model.addAttribute("topicsCommissionMember", topicsThatIncludeProfessorAsCommissionMember);
            model.addAttribute("topicsMentor", topicsThatIncludeProfessorAsMentor);

            model.addAttribute("professorOfInterest", professorOfInterest);
            model.addAttribute("topicCategories", TopicCategory.values());
            model.addAttribute("subCategories", topicService.getAllSubCategories());
        }

        if(professorOfInterest == null)
            model.addAttribute("professorsToBeShown", professorsToBeShown);

        else
            model.addAttribute("professorsToBeShown", professorsToBeShown
                    .stream()
                    .filter(professor -> professor.getId().equals(professorOfInterest.getId()))
                    .toList());

        model.addAttribute("showTopics", showTopics);

        model.addAttribute("startFrom",size*(Integer.parseInt(pageString)));
        model.addAttribute("title", "Професори");
        model.addAttribute("bodyContent", "professors-page");

        return "master-template";
    }

    @PostMapping
    public String filterByProfessor(Model model,
                                    @RequestParam String professorOfInterest)
    {
        if(!professorOfInterest.equalsIgnoreCase("Сите"))
            return getProfesorsPage(model, professorService.findById(professorOfInterest), null, null, null, null, "", 5);

        return getProfesorsPage(model, null, null, null, null, null, "", 5);
    }

    @GetMapping("/{id}/topics-list")
    public String showTopicsWhereProfessorIsMentionedSortedByMeetingDate(Model model,
                                                           @PathVariable String id)
    {
        Professor professorOfInterest = professorService.findById(id);
        List<Topic> filteredTopicsForProfessorOfInterest = professorService.findAllTopicsWhereProfessorIsMentionedSortedByMeetingDate(professorOfInterest);
        List<Topic> filteredTopicsForProfessorCommissionPresident = professorService.findAllTopicsWhereProfessorIsCommissionSortedByMeetingDate(professorOfInterest, CommissionMemberType.PRESIDENT);
        List<Topic> filteredTopicsForProfessorCommissionMember = professorService.findAllTopicsWhereProfessorIsCommissionSortedByMeetingDate(professorOfInterest, CommissionMemberType.MEMBER);
        List<Topic> filteredTopicsForProfessorMentor = professorService.findAllTopicsWhereProfessorIsCommissionSortedByMeetingDate(professorOfInterest, CommissionMemberType.MENTOR);

        return getProfesorsPage(model, professorOfInterest, filteredTopicsForProfessorOfInterest, filteredTopicsForProfessorCommissionPresident, filteredTopicsForProfessorCommissionMember,  filteredTopicsForProfessorMentor, "", 5);
    }

    @PostMapping("/{id}/topics-list")
    public String showTopicsForProfessorFilteredBySpecs(Model model,
                                                        @PathVariable String id,
                                                        @RequestParam(required = false) String categoryName,
                                                        @RequestParam(required = false) String subCategoryName)
    {
        Professor professorOfInterest = professorService.findById(id);
        List<Topic> filteredTopicsForProfessorOfInterest = professorService.findAllTopicsWhereProfessorIsMentionedFilteredByCategoryNameAndSubCategoryName(professorOfInterest, categoryName, subCategoryName);
        List<Topic> filteredTopicsForProfessorCommissionPresident = professorService.findAllTopicsWhereProfessorIsCommissionFilteredByCategoryNameAndSubCategoryName(professorOfInterest, CommissionMemberType.PRESIDENT, categoryName, subCategoryName);
        List<Topic> filteredTopicsForProfessorCommissionMember = professorService.findAllTopicsWhereProfessorIsCommissionFilteredByCategoryNameAndSubCategoryName(professorOfInterest, CommissionMemberType.MEMBER, categoryName, subCategoryName);
        List<Topic> filteredTopicsForProfessorMentor = professorService.findAllTopicsWhereProfessorIsCommissionFilteredByCategoryNameAndSubCategoryName(professorOfInterest, CommissionMemberType.MENTOR, categoryName, subCategoryName);

        if (!categoryName.equalsIgnoreCase("Сите"))
            model.addAttribute("selectedCategory", TopicCategory.valueOf(categoryName));

        model.addAttribute("selectedSubCategory", subCategoryName);

        return getProfesorsPage(model, professorOfInterest, filteredTopicsForProfessorOfInterest, filteredTopicsForProfessorCommissionPresident, filteredTopicsForProfessorCommissionMember, filteredTopicsForProfessorMentor,"", 5);
    }
}
