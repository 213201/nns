package mk.ukim.finki.wp.nns.repository;

import mk.ukim.finki.wp.nns.model.Room;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoomRepository extends JpaRepository<Room, String>
{

}
