package mk.ukim.finki.wp.nns.web;

import jakarta.servlet.http.HttpServletResponse;
import lombok.AllArgsConstructor;
import mk.ukim.finki.wp.nns.config.FacultyUserDetails;
import mk.ukim.finki.wp.nns.model.*;
import mk.ukim.finki.wp.nns.model.enumerations.CommissionMemberType;
import mk.ukim.finki.wp.nns.model.enumerations.TopicCategory;
import mk.ukim.finki.wp.nns.model.enumerations.TopicStatus;
import mk.ukim.finki.wp.nns.service.*;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.ui.Model;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.view.RedirectView;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@Controller
@RequestMapping(value = {"/topic"})
@AllArgsConstructor
public class TopicController {

    private final ProfessorService professorService;
    private final StudentService studentService;
    private final TopicService topicService;
    private final UserService userService;
    private final CommentSerivce commentService;
    private final TopicFileService topicFileService;
    private final MeetingService meetingService;


    @GetMapping("{meetingId}/add")
    public String getTopicCreatePage(Model model,
                                     @PathVariable String meetingId)
    {
        List<Student> students = studentService.findAll();
        List<Professor> professors = professorService.findAll();

        model.addAttribute("meetingId", meetingId);
        model.addAttribute("topicCategories", TopicCategory.values());
        model.addAttribute("students", students);
        model.addAttribute("professors", professors);
        model.addAttribute("bodyContent", "topic-add-page");

        return "master-template";
    }

    @PostMapping({"{meetingId}/add"})
    public String CreateTopic(@PathVariable String meetingId,
                              @RequestParam TopicCategory categoryName,
                              @RequestParam (required = false) String subCategoryName,
                              @RequestParam String serialNumber,
                              @RequestParam (required = false) String index,
                              @RequestParam (required = false) String professorId,
                              @RequestParam (required = false) String commissionPresidentId,
                              @RequestParam (required = false) String mentorId,
                              @RequestParam (required = false) List<String> commissionMembersIds,
                              @RequestParam(value = "file", required = false) MultipartFile file,
                              @RequestParam (required = false) String fileURL,
                              @RequestParam (defaultValue = "false") Boolean isVotable) throws IOException {
        byte[] fileContent = file.getBytes();
        String fileName = file.getOriginalFilename();
        topicService.create(categoryName, subCategoryName,serialNumber, meetingId, index, professorId, mentorId, commissionPresidentId, commissionMembersIds,fileContent,fileName, fileURL, isVotable);
        String meetingNumberEncoded = URLEncoder.encode(meetingId, StandardCharsets.UTF_8);
        meetingNumberEncoded = meetingNumberEncoded.replace("+", "%20");
        return "redirect:/nns-meetings/"+ meetingNumberEncoded +"/topics-list";
    }

    @GetMapping("{meetingId}/request")
    public String getTopicRequestPage(Model model,
                                     @PathVariable String meetingId)
    {
        List<Student> students = studentService.findAll();
        List<Professor> professors = professorService.findAll();

        model.addAttribute("meetingId", meetingId);
        model.addAttribute("topicCategories", TopicCategory.values());
        model.addAttribute("students", students);
        model.addAttribute("professors", professors);
        model.addAttribute("bodyContent", "topic-request-page");

        return "master-template";
    }

    @PostMapping({"{meetingId}/request"})
    public String RequestTopic(@PathVariable String meetingId,
                              @RequestParam TopicCategory categoryName,
                              @RequestParam (required = false) String subCategoryName,
                              @RequestParam String serialNumber,
                              @RequestParam (required = false) String index,
                              @RequestParam (required = false) String professorId,
                              @RequestParam (required = false) String commissionPresidentId,
                              @RequestParam (required = false) String mentorId,
                              @RequestParam (required = false) List<String> commissionMembersIds,
                              @RequestParam(value = "file", required = false) MultipartFile file,
                              @RequestParam (required = false) String fileURL,
                              @RequestParam (defaultValue = "false") Boolean isVotable,
                               Authentication token) throws IOException {
        Object principal = token.getPrincipal();
        FacultyUserDetails userAcc = (FacultyUserDetails) principal;
        String requestedById = userAcc.getUsername();

        byte[] fileContent = file.getBytes();
        String fileName = file.getOriginalFilename();
        topicService.request(categoryName, subCategoryName,serialNumber, meetingId, index, professorId,
                mentorId, commissionPresidentId, commissionMembersIds,fileContent,fileName, fileURL, requestedById, isVotable);
        String meetingNumberEncoded = URLEncoder.encode(meetingId, StandardCharsets.UTF_8);
        meetingNumberEncoded = meetingNumberEncoded.replace("+", "%20");
        return "redirect:/nns-meetings/"+ meetingNumberEncoded +"/topics-list";
    }

    @GetMapping("{topicId}/delete")
    public String showDeleteTopicPage(Model model,
                                      @PathVariable Long topicId)
    {
        Topic topic = topicService.findById(topicId);

        model.addAttribute("topic",topic);
        model.addAttribute("bodyContent", "topic-delete-page");

        return "master-template";
    }

    @PostMapping("{topicId}/delete")
    public String deleteTopic(@PathVariable Long topicId)
    {
        Topic topic = this.topicService.remove(topicId);

        String meetingNumberEncoded = topic.getMeeting().getMeetingNumberEncoded();
        meetingNumberEncoded = meetingNumberEncoded.replace("+", "%20");

        return "redirect:/nns-meetings/"+ meetingNumberEncoded +"/topics-list";
    }

    @GetMapping("{topicId}")
    public String getTopicDetails(Model model,
                                  @PathVariable Long topicId,
                                  Authentication token)
    {
        Object principal = token.getPrincipal();
        FacultyUserDetails userAcc = (FacultyUserDetails) principal;
        String userId = userAcc.getUsername();

        Topic topic = topicService.findById(topicId);
        User user = userService.findById(userId);
        Long previousTopicId = topicService.findPreviousId(topicId);
        Long followingTopicId = topicService.findFollowingId(topicId);

        Boolean hasComments = topic.getComments().stream()
                .anyMatch(comment -> comment.getProfessor().getId().equals(user.getId()));

        model.addAttribute("topic", topic);
        model.addAttribute("previousTopic", previousTopicId);
        model.addAttribute("followingTopic", followingTopicId);
        model.addAttribute("user", user);
        model.addAttribute("hasComments", hasComments);
        model.addAttribute("bodyContent", "topic-details-page");
        model.addAttribute("title", "Детали за точка");

        return "master-template";
    }

    @GetMapping("present/{topicId}")
    public String presentTopicDetails(Model model,
                                  @PathVariable Long topicId,
                                  Authentication token)
    {
        Object principal = token.getPrincipal();
        FacultyUserDetails userAcc = (FacultyUserDetails) principal;
        String userId = userAcc.getUsername();

        Topic topic = topicService.findById(topicId);
        User user = userService.findById(userId);
        Long previousTopicId = topicService.findPreviousId(topicId);
        Long followingTopicId = topicService.findFollowingId(topicId);

        String meetingId = topic.getMeeting().getMeetingNumber();
        List<Topic> topics = meetingService.sortAcceptedTopicsBySerialNumberForMeeting(meetingId);

        Boolean hasComments = topic.getComments().stream()
                .anyMatch(comment -> comment.getProfessor().getId().equals(user.getId()));

        model.addAttribute("topics", topics);
        model.addAttribute("topic", topic);
        model.addAttribute("previousTopic", previousTopicId);
        model.addAttribute("followingTopic", followingTopicId);
        model.addAttribute("user", user);
        model.addAttribute("hasComments", hasComments);
        model.addAttribute("bodyContent", "topic-presenting-page");
        model.addAttribute("title", "Детали за точка");

        return "master-template";
    }


    @GetMapping("{topicId}/edit")
    public String showTopicEditPage(Model model,
                                    @PathVariable Long topicId,
                                    Authentication token)
    {
        Object principal = token.getPrincipal();
        FacultyUserDetails userAcc = (FacultyUserDetails) principal;
        String userId = userAcc.getUsername();

        Topic topic = topicService.findById(topicId);
        List<Student> students = studentService.findAll();
        List<Professor> professors = professorService.findAll();
        User user = userService.findById(userId);

        List<Professor> commission = topic.getCommissionMembers().stream()
                .filter(commissionMember -> commissionMember.getType() == CommissionMemberType.MEMBER)
                .map(CommissionMember::getProfessor)
                .toList();

        CommissionMember president= topic.getCommissionMembers().stream()
                .filter(commissionMember -> commissionMember.getType() == CommissionMemberType.PRESIDENT)
                .findFirst().orElse(null);

        if(president != null) {
            Professor commissionPresident = president.getProfessor();
            model.addAttribute("president", commissionPresident);
        }

        Optional<CommissionMember> mentorCommissionObj = topic.getCommissionMembers().stream()
                .filter(commissionMember -> commissionMember.getType() == CommissionMemberType.MENTOR)
                .findFirst();

        Professor mentor = mentorCommissionObj.map(CommissionMember::getProfessor).orElse(null);

        model.addAttribute("topic", topic);
        model.addAttribute("students", students);
        model.addAttribute("professors", professors);
        model.addAttribute("user", user);
        model.addAttribute("commission", commission);

        model.addAttribute("mentor", mentor);
        model.addAttribute("topicCategories", TopicCategory.values());
        model.addAttribute("topicStatuses", TopicStatus.values());
        model.addAttribute("title", "Измена на точка");
        model.addAttribute("bodyContent", "topic-edit-page");

        return "master-template";
    }

    @PostMapping({"{topicId}/edit"})
    public String EditTopic(@PathVariable Long topicId,
                            @RequestParam TopicCategory categoryName,
                            @RequestParam TopicStatus topicStatus,
                            @RequestParam(required = false) String subCategoryName,
                            @RequestParam(required = false) Integer acceptNumber,
                            @RequestParam(required = false) Integer againstNumber,
                            @RequestParam(required = false) Integer sustainedNumber,
                            @RequestParam String description,
                            @RequestParam String serialNumber,
                            @RequestParam(required = false) Boolean isAccepted,
                            @RequestParam(required = false) String discussion,
                            @RequestParam(required = false) String index,
                            @RequestParam(required = false) String professorId,
                            @RequestParam(required = false) String mentorId,
                            @RequestParam (required = false) String commissionPresidentId,
                            @RequestParam (required = false) List<String> commissionMembersIds,
                            @RequestParam(value = "file", required = false) MultipartFile file,
                            @RequestParam (required = false) String fileURL,
                            @RequestParam (defaultValue = "false") Boolean isVotable) throws IOException
    {
        Topic topic = topicService.findById(topicId);
        byte[] fileContent = null;
        String fileName = null;

        if (file != null && !file.isEmpty()) {
            fileContent = file.getBytes();
            fileName = file.getOriginalFilename();
        }

        topicService.update(topicId, categoryName, topicStatus,
                subCategoryName, acceptNumber, againstNumber, sustainedNumber,
                description, serialNumber, isAccepted,
                discussion, topic.getMeeting().getMeetingNumber(), index,
                professorId, mentorId, commissionPresidentId, commissionMembersIds,fileContent,fileName, fileURL, isVotable);

        String meetingNumberEncoded = topic.getMeeting().getMeetingNumberEncoded();
        meetingNumberEncoded = meetingNumberEncoded.replace("+", "%20");

        return topic.getTopicStatus() == TopicStatus.REQUESTED
                ? "redirect:/nns-meetings/"+ meetingNumberEncoded +"/requested-topics-list"
                : "redirect:/nns-meetings/"+ meetingNumberEncoded +"/topics-list";
    }

    @PostMapping({"{topicId}/editPresent"})
    public String EditTopicPresent(@PathVariable Long topicId,
                            @RequestParam(required = false) Integer acceptNumber,
                            @RequestParam(required = false) Integer againstNumber,
                            @RequestParam(required = false) Integer sustainedNumber,
                            @RequestParam(required = false) String discussion) throws IOException
    {
        Topic topic = topicService.findById(topicId);

        topicService.updateFromPresentingMode(topicId, acceptNumber, againstNumber, sustainedNumber, discussion);
        Long nextTopicId = topicService.findFollowingId(topicId);

        if(nextTopicId != null)
            return "redirect:/topic/present/" + nextTopicId;

        String meetingNumberEncoded = topic.getMeeting().getMeetingNumberEncoded();
        meetingNumberEncoded = meetingNumberEncoded.replace("+", "%20");


        return "redirect:/nns-meetings/"+ meetingNumberEncoded +"/topics-list";
    }

    @GetMapping("{topicId}/add-comment")
    public String addCommentToTopic(Model model,
                                    @PathVariable Long topicId)
    {
        Topic topic = this.topicService.findById(topicId);

        model.addAttribute("topic", topic);
        model.addAttribute("title", "Додавање коментар");
        model.addAttribute("bodyContent", "topic-add-comment-page");

        return "master-template";
    }

    @PostMapping("{topicId}/add-comment")
    public String addComentToTopic(@PathVariable Long topicId,
                                   @RequestParam String commentText,
                                   Authentication token)
    {
        Object principal = token.getPrincipal();
        FacultyUserDetails userAcc = (FacultyUserDetails) principal;
        String userId = userAcc.getUsername();

        commentService.create(commentText, topicId, userId);

        Topic topic = topicService.findById(topicId);

        String meetingNumberEncoded = topic.getMeeting().getMeetingNumberEncoded();
        meetingNumberEncoded = meetingNumberEncoded.replace("+", "%20");

        return "redirect:/nns-meetings/"+ meetingNumberEncoded +"/topics-list";
    }
    @GetMapping("edit-comment/{commentId}")
    public String editCommentForTopic(Model model,
                                    @PathVariable Long commentId)
    {
        Comment comment = this.commentService.findById(commentId).orElseThrow();

        model.addAttribute("comment", comment);
        model.addAttribute("title", "Измена на коментар");
        model.addAttribute("bodyContent", "topic-edit-comment-page");

        return "master-template";
    }

    @PostMapping("edit-comment/{commentId}")
    public String editCommentForTopic(@PathVariable Long commentId,
                                   @RequestParam String commentText,
                                   Authentication token)
    {
        Object principal = token.getPrincipal();
        FacultyUserDetails userAcc = (FacultyUserDetails) principal;
        String userId = userAcc.getUsername();

        Comment comment = commentService.findById(commentId).orElseThrow();

        if(comment.getProfessor().getId().equals(userId))
        {
            commentService.update(commentId, commentText);
        }
        return "redirect:/topic/" + comment.getTopic().getId();
    }
    @GetMapping("delete-comment/{commentId}")
    public String deleteCommentForTopic(Model model,
                                      @PathVariable Long commentId)
    {
        Comment comment = this.commentService.findById(commentId).orElseThrow();

        model.addAttribute("comment", comment);
        model.addAttribute("title", "Бришење на коментар");
        model.addAttribute("bodyContent", "topic-delete-comment-page");

        return "master-template";
    }

    @PostMapping("delete-comment/{commentId}")
    public String deleteCommentForTopic(@PathVariable Long commentId,
                                      Authentication token)
    {
        Object principal = token.getPrincipal();
        FacultyUserDetails userAcc = (FacultyUserDetails) principal;
        String userId = userAcc.getUsername();

        Comment comment = commentService.findById(commentId).orElseThrow();

        if(comment.getProfessor().getId().equals(userId))
        {
            commentService.delete(commentId);
        }
        return "redirect:/topic/" + comment.getTopic().getId();
    }


    @GetMapping("{topicId}/download-file")
    @ResponseBody
    public void downloadTopicFile(@PathVariable Long topicId, HttpServletResponse response){
        Topic topic = topicService.findById(topicId);
        Optional<TopicFile> topicFileOptional = topicFileService.findById(topic.getFile().getId());
        if (topicFileOptional.isPresent()) {
            TopicFile topicFile = topicFileOptional.get();
            String encodedFilename = URLEncoder.encode(topicFile.getFileName(), StandardCharsets.UTF_8);

            response.setContentType("application/octet-stream; charset=UTF-8");
            response.setHeader("Content-Disposition", "attachment; filename=\"" + encodedFilename + "\"");

            try (OutputStream outputStream = response.getOutputStream()) {
                outputStream.write(topicFile.getContent());
                outputStream.flush();
            } catch (IOException e) {
                // Handle exception
                e.printStackTrace();
            }
        } else {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        }
    }

    @GetMapping("{topicId}/link-file")
    public RedirectView redirectToExternalUrl(@PathVariable Long topicId) {
        Topic topic = this.topicService.findById(topicId);
        String externalUrl = topic.getFileURL();

        RedirectView redirectView = new RedirectView();
        redirectView.setUrl(externalUrl);
        return redirectView;
    }

    @GetMapping("{topicId}/delete-file")
    public String deleteFile(Model model,
                             @PathVariable Long topicId,
                             Authentication token)
    {
        Topic topic = topicService.removeFile(topicId);

//        showTopicEditPage(model, topicId, token);

        return ("redirect:/topic/" + topic.getId() + "/edit");
    }

    @GetMapping("{topicId}/approve")
    public String approveTopic(Model model,
                             @PathVariable Long topicId)
    {
        Topic topic = topicService.approve(topicId);

        String meetingNumberEncoded = topic.getMeeting().getMeetingNumberEncoded();
        meetingNumberEncoded = meetingNumberEncoded.replace("+", "%20");

        return "redirect:/nns-meetings/"+ meetingNumberEncoded +"/requested-topics-list";
    }
}
