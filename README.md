# Management of Teaching and Scientific Meetings

This project, developed using Spring, Thymeleaf, and PostgreSQL, serves as a comprehensive tool for managing teaching and scientific meetings at **Faculty of Computer Science and Engineering - Skopje**. It integrates with a common model to access faculty data efficiently.

## Features

### CRUD Operations
- **Meetings**: Create, read, update, and delete meetings. Each meeting can have multiple topics discussed.
- **Topics**: Manage topics within meetings, allowing CRUD operations and detailed interaction features for professors.

### Professor Interaction
- **Comments and Files**: Professors can leave comments on topics, upload/download files, and share document links directly related to meeting discussions.
- **Permissions**: Role-based access control ensures that only meeting holders have full CRUD capabilities, maintaining data integrity and security.

### Tabular Display and Filtering
- **Meetings Tab**: View all past and future meetings in a paginated table format (5 meetings per page). Use Pageable to navigate through meetings and filter them by date range.
- **Professors Tab**: Browse through a paginated list of professors, with the ability to filter by their involvement in topics (e.g., mentions, commissions, mentoring). Detailed views provide insights into specific topics relevant to each professor.
- **Students Tab**: Similar to the Professors tab but focused on student involvement in topics, limited to mentions without commission or mentoring roles.

### Dockerized Deployment
- **Seamless Deployment**: Utilizes Docker and Docker Compose to eliminate environment inconsistencies. Follow the steps below to set up the application.

## Accessing the Application

### Prerequisites
- Ensure the COMMON MODEL application is running to set up the necessary database schemas.
- Docker

### Steps to Run

1. **Start COMMON MODEL Application**:
    - Navigate to the directory of your COMMON MODEL project.
    - Initialize the necessary services to create the database schema:
      ```
      cd path/to/common-model
      docker-compose up -d
      ```

2. **Initialize Database for TSM Application**:
    - Navigate to the directory of your TSM (Teaching and Scientific Meetings) project.
    - Launch the PostgreSQL container for the faculty database using Docker Compose:
      ```
      cd path/to/tsm-app
      docker-compose up -d finki-db
      ```

3. **Run the TSM Application**:
    - Create a `.env` file in the TSM project root with the following configuration:
      ```
      DB_PASSWORD=commonModelPassword
      ```
    - Build and start the Docker containers for the TSM application:
      ```
      docker-compose up -d --build
      ```

4. **Access the TSM Application**:
    - Open your web browser and navigate to:
      ```
      http://localhost:8000
      ```

## Inserting Data into the `student` Table

### Access PostgreSQL Database

1. **Open your terminal or command prompt.**
2. **Connect to the PostgreSQL database using the `psql` command:**

   ```bash
   psql -U finki_admin -d finki-services-db
   ```

Once connected to the database, you can insert data using the INSERT INTO SQL command.

``Here’s an example of inserting multiple rows into the student table:``

```bash
INSERT INTO student (student_index, email, last_name, name, parent_name, study_program_code)
VALUES
    ('213201', 'jane.panov@students.finki.ukim.mk', 'Panov', 'Jane', 'Vlatko', 'PIT23'),
    ('213016', 'hristijan.stefov@students.finki.ukim.mk', 'Stefov', 'Hristijan', 'Viktor', 'PIT23'),
    ('213079', 'panche.prendjov@students.finki.ukim.mk', 'Prendjov', 'Panche', 'Baze', 'PIT23'),
    ('213118', 'matej.todorovski@students.finki.ukim.mk', 'Todorovski', 'Matej', 'Test', 'PIT23');
```

3. **Verify Data Insertion**

- After executing the INSERT statement, you should see a confirmation message indicating the number of rows inserted (INSERT 0 4 for four rows in this example).

4. **Commit Changes**

- By default, PostgreSQL transactions are committed automatically after each SQL command that modifies data. If you need to manually commit changes (in case of explicit transactions), use the COMMIT; command.

5. **Disconnect from PostgreSQL**

To disconnect from the PostgreSQL database, type \q and press Enter.
## Notes
- Ensure the COMMON MODEL application is running before starting both the database container and the TSM application container to establish proper network connections.

## Contributors
- Hristijan Stefov
- Panche Prendjov
- Matej Todorovski
- Jane Panov
